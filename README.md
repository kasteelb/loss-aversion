# Loss Aversion

This project contains a jupyter notebook with some ideas on loss aversion. It is intended as a draft for a possible future article or blogpost. Feel free to contribute or copy.

#### Running the notebook
You should have Python 3 installed. Then, the setup is as follows:
1. Clone the project with `git clone [project-url]`
2. Go to the project folder in a terminal and create a virtual environment with `python3 -m venv [virtual env name]`
3. Activate the environment with `source [virtual env name]/bin/activate` (on Mac, Windows may differ)
4. Install jupyter: `pip install jupyter`
5. Run the notebook server with the command `jupyter-notebook`.
